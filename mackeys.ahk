SetTitleMatchMode, 2
#NoTrayIcon
#SingleInstance, force

; --- General ---

; App

$^!q::Send !{F4}
LCtrl & Tab::AltTab


; Cursor (may be incompatible with games)

#If !WinActive("ahk_exe DNF.exe")

!Left::Send ^{Left}
!Right::Send ^{Right}

^Left::Send {Home}
^Right::Send {End}

^Backspace::Send +{Home}{Delete}
!Backspace::Send +^{Left}{Delete}

#If

; Input method
$^Space::Send {LCtrl Down}{LShift Down}{LShift Up}{LCtrl Up}