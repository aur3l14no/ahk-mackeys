# AHK Mackeys

## Usage

1. Use `KeyTweak` to make the bottom-left keys `LCtrl` `LAlt` `LCtrl`
2. Change `Switch Input Language` key combination to `LCtrl + LShift`
2. Run `mackeys.ahk` as admin`
3. (Optional) Compile `mackeys.ahk` and make the executable run at startup using `Task Scheduler` (select "Run with highest privileges")
